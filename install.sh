#! /bin/bash

dir=$(pwd)"/darknet"
git clone https://github.com/pjreddie/darknet.git && cd darknet && make && echo "Installation complete"
echo "Getting weights"
wget https://pjreddie.com/media/files/yolov3.weights -P $dir
echo "Getting images"
wget https://pjreddie.com/media/files/VOCtrainval_11-May-2012.tar -P $dir 
wget https://pjreddie.com/media/files/VOCtrainval_06-Nov-2007.tar -P $dir
wget https://pjreddie.com/media/files/VOCtest_06-Nov-2007.tar -P $dir
#tar xf $directory"VOCtrainval_11-May-2012.tar" -C $direcotry
#tar xf VOCtrainval_06-Nov-2007.tar
#tar xf VOCtest_06-Nov-2007.tar

